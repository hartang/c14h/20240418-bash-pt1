---
title: Bash und Ich (Teil 1)
subtitle: Wie ich Bash-Skripte anfange
author: Andreas Hartmann (@hartan)
patat:
  breadcrumbs: true
  wrap: true
  slideLevel: 2
  margins:
    left: 5
    right: 5
    top: 1
    bottom: 1
  eval:
    figlet:
      command: figlet -s -w 70 -f big
      fragment: false
      replace: true
...

```figlet
Bash und Ich (Teil 1)
```


# Intro

## Dies ist kein Grundlagenvortrag

- Grundlegende Kenntnisse über bash werden vorausgesetzt
- Online findet man im Zweifel genügend Quellen


## Unser Thema heute

```bash
#!/usr/bin/env bash
set -euo pipefail
```

# Shebang

## Shebang

```bash
#!/usr/bin/env bash
```

- Legt fest, wie das Skript ausgeführt wird
- `env` sucht nach der ersten `bash` in `$PATH`

. . .

- Shebang kann Skripte z.B. auch in Containern starten:

```bash
#!/usr/bin/env -S podman run ...
echo "contents in '/etc'"
ls -la /etc
```


## Shebang

- *Frage*: Wieso nicht einfach:
  ```bash
  #!/bin/bash  
  ```

. . .

- Wegen Kompatibilität!
    - Nicht alle \*nix-Systeme legen die Bash dort ab
    - Unter MacOS ist die bash dort uralt (Lizenzkonflikt)
    - Erlaubt dem Nutzer, die `bash` festzulegen


# Shell-Optionen

## -e: errexit

```bash
set -e
```

- Bricht Skript beim ersten Fehler ab
- Vermeidet ,,Fehlerfortpflanzung''

```bash
#!/usr/bin/env bash

false
echo "Katastrophe voraus!"
```


## -e: errexit

- Bricht **nicht immer** sofort ab
- Ausnahmen:
    - `if`/`elif`
    - `while`/`until`
    - `&&`/`||` (außer am Ende)
    - `|` (außer am Ende)


## -u: nounset

```bash
set -u
```

- Zugriff auf unzugewiesene Variablen ist Fehler
- Standardverhalten: enthält leeren String (`""`)
- Vermeidet tödliche Tippfehler

. . .

- *Deckt ungeprüfte Annahmen auf*

> Auf unseren Entwicklerrechnern ist `$FOO` immer gesetzt ...


## -u: nounset

Beispiel:
```bash
#!/usr/bin/env bash

if [[ $# -gt 0 ]]; then
    VERY_DESCRIPTIVE_VARIABLE_NAME="$1"
    # ...
    ls "$HOME/$VERY_DESCRIPTIVE_VARABLE_NAME"
fi
```


## -u: nounset

- *Hinweis*: Prüfen auf leere Variablen ist Fehler!

```bash
set -u
if [[ -z "$FOO" ]]; then echo "'FOO' is empty"; fi
```

. . .

- *Lösung*: Leeren default angeben

```bash
set -u
if [[ -z "${FOO:-""}" ]]; then # ...
```


## -o pipefail

- Bricht pipeline ab, sobald ein Befehl fehlerhaft ist
- Am besten in Kombination mit `set -e`
- Standardverhalten:
    - Mach einfach weiter
    - Zur Not mit *leerem* Input

```bash
#!/usr/bin/env bash

# Hinweis: 'OS_NAME' existiert nicht als Key
OSNAME="$(grep "OS_NAME=" /etc/os-release | cut -d'"' -f2)"
echo "your OS is '$OSNAME'"
```


## *Bonus* -x: xtrace

- "debug"-Modus
- Zeigt alle ausgeführten Befehle an (auf stdout)
- Ersetzt Variablen in Befehlen mit "echtem" Wert
- **Sehr nützlich z.B. für CI-Skripte**


## Fazit

- `bash` programmieren *kann* schmerzhaft sein...

. . .

- ...aber das muss es nicht sein!
    - Shebang setzen für Kompatibilität
      ```bash
      #!/usr/bin/env bash
      ```
    - Shell-Optionen setzen zur frühen Fehlerdetektion
      ```bash
      set -euo pipefail
      ```


# Fragen?

```figlet
EOF
```

#!/usr/bin/env bash

# Hinweis: 'OS_NAME' existiert nicht als Key
OSNAME="$(grep "OS_NAME=" /etc/os-release | cut -d'"' -f2)"
echo "your OS is '$OSNAME'"


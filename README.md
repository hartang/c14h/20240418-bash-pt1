# Bash und Ich - Wie ich meine Bash-Skripte beginne

Die meisten werden wohl schon mal in irgendeiner Form ein Bash-Skript
geschrieben haben - und wenn es nur in einem CI-System war. Dabei gibt es ein
paar böse Fallen, die einem das Leben auf subtile Art und Weise sehr schwer
machen.

Ich möchte euch heute vorstellen, wie ich meine Bash-Skripte beginne und wie
man schon mit den ersten zwei Zeilen viele fatale Fehlerquellen beseitigt.

Hier befinden sich die "Folien" zu einem Vortrag, den ich am 18.04.2024 im
Rahmen der c14h beim NoName e.V. gehalten habe. Die Aufnahme zum Vortrag ist
hier verlinkt: https://www.noname-ev.de/chaotische_viertelstunde.html#c14h_614


## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
